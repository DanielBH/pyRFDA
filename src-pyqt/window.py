# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'window.ui'
#
# Created: Wed Sep 21 22:55:00 2016
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_window(object):
    def setupUi(self, window):
        window.setObjectName(_fromUtf8("window"))
        window.resize(579, 236)
        self.gridLayout_3 = QtGui.QGridLayout(window)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.gridLayout_2 = QtGui.QGridLayout()
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label_mass = QtGui.QLabel(window)
        self.label_mass.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_mass.setObjectName(_fromUtf8("label_mass"))
        self.gridLayout.addWidget(self.label_mass, 3, 0, 1, 1)
        self.label_width = QtGui.QLabel(window)
        self.label_width.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_width.setObjectName(_fromUtf8("label_width"))
        self.gridLayout.addWidget(self.label_width, 1, 0, 1, 1)
        self.label_thickess = QtGui.QLabel(window)
        self.label_thickess.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_thickess.setObjectName(_fromUtf8("label_thickess"))
        self.gridLayout.addWidget(self.label_thickess, 2, 0, 1, 1)
        self.label_length = QtGui.QLabel(window)
        self.label_length.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_length.setObjectName(_fromUtf8("label_length"))
        self.gridLayout.addWidget(self.label_length, 0, 0, 1, 1)
        self.doubleSpinBox_length = QtGui.QDoubleSpinBox(window)
        self.doubleSpinBox_length.setDecimals(4)
        self.doubleSpinBox_length.setProperty("value", 1.0)
        self.doubleSpinBox_length.setObjectName(_fromUtf8("doubleSpinBox_length"))
        self.gridLayout.addWidget(self.doubleSpinBox_length, 0, 1, 1, 1)
        self.doubleSpinBox_width = QtGui.QDoubleSpinBox(window)
        self.doubleSpinBox_width.setDecimals(4)
        self.doubleSpinBox_width.setProperty("value", 1.0)
        self.doubleSpinBox_width.setObjectName(_fromUtf8("doubleSpinBox_width"))
        self.gridLayout.addWidget(self.doubleSpinBox_width, 1, 1, 1, 1)
        self.doubleSpinBox_thickness = QtGui.QDoubleSpinBox(window)
        self.doubleSpinBox_thickness.setDecimals(4)
        self.doubleSpinBox_thickness.setProperty("value", 1.0)
        self.doubleSpinBox_thickness.setObjectName(_fromUtf8("doubleSpinBox_thickness"))
        self.gridLayout.addWidget(self.doubleSpinBox_thickness, 2, 1, 1, 1)
        self.doubleSpinBox_mass = QtGui.QDoubleSpinBox(window)
        self.doubleSpinBox_mass.setDecimals(4)
        self.doubleSpinBox_mass.setProperty("value", 1.0)
        self.doubleSpinBox_mass.setObjectName(_fromUtf8("doubleSpinBox_mass"))
        self.gridLayout.addWidget(self.doubleSpinBox_mass, 3, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.pushButton_bending = QtGui.QPushButton(window)
        self.pushButton_bending.setObjectName(_fromUtf8("pushButton_bending"))
        self.horizontalLayout.addWidget(self.pushButton_bending)
        self.pushButton_torsion = QtGui.QPushButton(window)
        self.pushButton_torsion.setObjectName(_fromUtf8("pushButton_torsion"))
        self.horizontalLayout.addWidget(self.pushButton_torsion)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pushButton_quit = QtGui.QPushButton(window)
        self.pushButton_quit.setObjectName(_fromUtf8("pushButton_quit"))
        self.horizontalLayout.addWidget(self.pushButton_quit)
        self.gridLayout_2.addLayout(self.horizontalLayout, 2, 0, 1, 1)
        self.label_status = QtGui.QLabel(window)
        self.label_status.setAlignment(QtCore.Qt.AlignCenter)
        self.label_status.setObjectName(_fromUtf8("label_status"))
        self.gridLayout_2.addWidget(self.label_status, 1, 0, 1, 1)
        self.gridLayout_3.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.retranslateUi(window)
        QtCore.QMetaObject.connectSlotsByName(window)

    def retranslateUi(self, window):
        window.setWindowTitle(_translate("window", "pyRFDA", None))
        self.label_mass.setText(_translate("window", "Sample mass", None))
        self.label_width.setText(_translate("window", "Sample width", None))
        self.label_thickess.setText(_translate("window", "Sample thickness", None))
        self.label_length.setText(_translate("window", "Sample length", None))
        self.doubleSpinBox_length.setSuffix(_translate("window", " m", None))
        self.doubleSpinBox_width.setSuffix(_translate("window", " m", None))
        self.doubleSpinBox_thickness.setSuffix(_translate("window", " m", None))
        self.doubleSpinBox_mass.setSuffix(_translate("window", " kg", None))
        self.pushButton_bending.setText(_translate("window", "Run bending test", None))
        self.pushButton_torsion.setText(_translate("window", "Run torsion test", None))
        self.pushButton_quit.setText(_translate("window", "Quit", None))
        self.label_status.setText(_translate("window", "Status", None))

