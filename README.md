Welcome to the android version of pyrfda !!!

The Resonant Frequency Damping Analysis (RFDA) is a smart method to measure the elasticity of brittle materials such as Young's modulus and shear modulus. The RFDA method uses the sound produced by the natural vibrations of an impacted piece of material to evaluate its elastic properties.  To promote bending or torsion modes, the sample, the impactor and the microphone must be placed at right places. Some pictures are embedded in this software to help in implementing the experimental protocol.

The RFDA method is also known as "Impulse Excitation Technique". To get more informations, you can visit the Wikipedia page about this method : https://en.wikipedia.org/wiki/Impulse_excitation_technique

This software is distributed under the free GNU-GPL v3 license.  The original artworks are distributed under the Creative commons license (BY NC SA). Copyright 2016-2020, Damien André. My homepage is http://www.unilim.fr/pages_perso/damien.andre/


ENJOY !
