# This file is part of the pyrfda software, a software to measure elasticity constants
#
# Author(s): - Damien ANDRE     SPCTS/ENS Ceramique industrielle, Limoges France
#            <damien.andre@unilim.fr>
#
# Copyright (C) 2016-2020 Damien ANDRE
#

#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:www.gnu.org/licenses/>.




from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import StringProperty
from kivy.clock import Clock
from kivy.utils import platform
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from kivy.uix.image import Image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.behaviors import ButtonBehavior
from kivy.garden.graph import Graph, MeshLinePlot
from kivy.graphics import Line
from kivy.metrics import sp
from functools import partial
from kivy.storage.jsonstore import JsonStore
from kivy.core.window import Window

#To automaticaaly translate virtual keyboard
Window.softinput_mode = 'below_target'

import importlib
import os


from numpy import fromstring as np_fromstring
from numpy import int16 as np_int16
from numpy import hstack as np_hstack
from numpy import abs as np_abs
from numpy.fft import fft as  np_fft
from numpy import linspace as np_linspace
from numpy import max as np_max

import traceback
from raven import Client


########### INPUT SETTINGS ###########
RATE=44100        # <- capture frequency
DELAY = 5         # <- delay before capturing
CHUNKSIZE = 1024  # <- buffer size
RECORDTIME = 2    # <- recording time in second 
SAMPLE_PER_SECOND = 60.
######################################



# Create both screens. Please note the root.manager.current: this is how
# you can control the ScreenManager from kv. Each screen has by default a
# property manager that gives you the instance of the ScreenManager used.
Builder.load_file('./win.kv')



def showHelpPage():
    import webbrowser
    webbrowser.open('http://www.yakuru.fr/~pyrfda/pyRFDA/src-kivy/data/help.html')

    # Arg, Can't open local html file !! :((
    # filename = './data/help.html'
    # path = 'file://' + os.path.realpath(filename)
    # webbrowser.open_url(path)
    


class Sample:
    def __init__(self):
        self.width = None
        self.length = None
        self.thickness = None
        self.mass = None
        self.xf = []
        self.yf = []
        self.mode = ''

    def set_value(self, attr, val):
        try:
            num = float(val)
            if (num <= 0.):
                setattr(self, attr, None)
                return 'Error : the ' + attr + ' value must be higher than 0'
            setattr(self, attr, num)
            return 'Setting ' + attr + ' to ' + val 
            return True
        except ValueError:
            setattr(self, attr, None)
            return 'Error : the ' + attr + ' value is invalid'


    def ok(self):
        return self.width is not None and self.length is not None and self.thickness is not None and self.mass is not None

    def compute_young_modulus(self, freq):
        b = self.width  
        t = self.thickness
        L = self.length
        m = self.mass
        fb = freq
        T = 1. + 6.585*(t/L)**2.
        E = 0.9465*(m*(fb**2)/b)*((L/t)**3.)*T
        return E

    def compute_shear_modulus(self, freq):
        b = self.width  
        t = self.thickness
        L = self.length
        m = self.mass
        ft = freq
        B = ( (b/t)+(t/b) ) / ( 4.*(t/b) - 2.52*((t/b)**2) + 0.21*((t/b)**6) )
        A = (0.5062 - 0.8776*(b/t) + 0.3504*((b/t)**2) - 0.0078*((b/t)**3)) / ( 12.03*(b/t) + 9.892*((b/t)**2))
        G = ( (4.*L*m*(ft**2)) / (b*t) ) * (B/(1+A))
        return G

    def save(self, name):
        store = JsonStore(name)
        store.put('sample', width=self.width, thickness = self.thickness,
                  length = self.length, mass = self.mass )

    def load_file(self, name):
        store = JsonStore(name)
        self.width = float(store.get('sample')['width'])
        self.thickness  = float(store.get('sample')['thickness'])
        self.length  = float(store.get('sample')['length'])
        self.mass  = float(store.get('sample')['mass'])


class StatusLabel(ButtonBehavior, Label):
    title = StringProperty('')
    def __init__(self, **kwargs):
        super(StatusLabel, self).__init__(**kwargs)
        self.bind(on_press=self.zoom)
        self.msg = self.text
        self.popup = None

    def zoom(self, arg1, exit_button = True):
        if (self.popup is not None):
            self.popup.dismiss()
        box = BoxLayout(orientation= "vertical")
        lbl = Label(text=self.msg, halign='left', valign='top', size_hint=(1., .9), markup=True)
        box.add_widget(lbl)
        if exit_button:
            but = Button(text='ok', size_hint=(1., .1))
            box.add_widget(but)
        self.popup = Popup(title=self.title, size_hint=(0.8, 0.8), content=box)
        self.popup.bind(on_dismiss=self.on_popup_close)
        lbl.bind(size=lambda s, w: s.setter('text_size')(s, w))
        if exit_button:
            but.bind(on_press=self.popup.dismiss)
        self.popup.open()

    def on_popup_close(self, instance):
        self.popup = None

    def set_msg(self, msg):
        self.msg = msg
        self.text = '[b]' + self.title + '[/b]\n' + msg

class MyGraph(ButtonBehavior, Graph):
    def __init__(self, **kwargs):
        super(MyGraph, self).__init__(**kwargs)

class MyTextInput(TextInput):
    def __init__(self, **kwargs):
        super(MyTextInput, self).__init__(**kwargs)


class NavButton(Widget):
    def __init__(self, **kwargs):
        super(NavButton, self).__init__(**kwargs)



class Screen_0(Screen):
    def __init__(self, sample, **kwargs):
        super(Screen_0, self).__init__(**kwargs)
        self.nav.button_prev.disabled=True
        self.nav.button_next.disabled=True
        self.nav.button_next.bind(on_press=self.next_screen)
        self.status.set_msg('Enter sample properties: length, width, thickness and mass. In addition, you can load or save sample properties from files.')
        self.sample = sample
        self.nav.help.bind(on_press=self.help)


    def help(self, obj):
        showHelpPage()

    def next_screen(self, obj):
        self.manager.transition.direction = 'left'
        self.manager.current = 'Screen_1'
        

    def set_sample_value(self, focus, attr, val):
        if focus == False: #unfocused
            self.status.set_msg(self.sample.set_value(attr, val))
        self.check_sample_parameter()


    def check_sample_parameter(self):
        self.sample.set_value('length', self.length_input.text)
        self.sample.set_value('width', self.width_input.text)
        self.sample.set_value('thickness', self.thickness_input.text)
        self.sample.set_value('mass', self.mass_input.text)
        if self.sample.ok():
            self.nav.button_next.disabled=False
            self.status.set_msg('Sample parameters are ok, you can go to the next step to record.')
        else:
            self.nav.button_next.disabled=True

    def save(self):
        txt = MyTextInput(multiline=False)
        popup = Popup(title='Enter file name and press enter', 
                      content=txt, size_hint=(0.6, None), height=sp(34*3))
        txt.bind(on_text_validate=partial(self.save_as, popup))
        self.save_input = txt
        popup.open()

    def save_as(self, popup, obj):
        name = self.save_input.text
        for file_name in os.listdir('./sample/'):
            if file_name == name:
                lay = BoxLayout(orientation='vertical')
                msg = Label(text='Sorry this file already exist')
                btt = Button(text='Remove the file',size_hint=(1, 0.2))
                lay.add_widget(msg)
                lay.add_widget(btt)

                notify = Popup(title='Problem', content=lay,  
                               size_hint=(0.6, 0.4))
                btt.bind(on_press=partial(self.rm_file, file_name, notify))
                
                notify.open()

        if (not os.path.isfile('./sample/' + name)):
            self.sample.save('./sample/' + name)
            popup.dismiss()
    
    def rm_file(self, file_name, popup, obj):
        os.remove('./sample/' + file_name)
        popup.dismiss()

    
    def load(self):
        lay = BoxLayout(orientation='vertical')
        num = len(os.listdir('./sample/'))
        hei = num*0.2 if num <= 4 else 0.8
        pop = Popup(title='Load file...', content=lay, size_hint=(0.6, hei))
        for file_name in os.listdir('./sample/'):
            button = ToggleButton(text=file_name, group='file')
            lay.add_widget(button)
            button.bind(on_press=partial(self.load_file, pop, button.text))
        pop.open()

    def load_file(self, popup, file_name, obj):
        self.sample.load_file('./sample/'+file_name)
        self.length_input.text = str(sample.length)
        self.width_input.text = str(sample.width)
        self.thickness_input.text = str(sample.thickness)
        self.mass_input.text = str(sample.mass)
        if popup is not None:
            popup.dismiss()
        self.check_sample_parameter()

    def remove(self):
        lay = BoxLayout(orientation='vertical')
        num = len(os.listdir('./sample/'))
        hei = num*0.2 if num <= 4 else 0.8
        pop = Popup(title='Remove file...', content=lay, size_hint=(0.6, hei))

        for file_name in os.listdir('./sample/'):
            button = ToggleButton(text=file_name, group='file')
            lay.add_widget(button)
            button.bind(on_press=partial(self.remove_file, pop, button.text))
        pop.open()
        
    def remove_file(self, popup, file_name, obj):
        os.remove('./sample/' + file_name)
        popup.dismiss()
    

class Screen_1(Screen):
    def __init__(self, sample, **kwargs):
        super(Screen_1, self).__init__(**kwargs)
        self.nav.button_next.bind(on_press=self.next_screen)
        self.nav.button_prev.bind(on_press=self.prev_screen)
        self.nav.button_prev.disabled=False
        self.nav.button_next.disabled=True

        msg = 'Put the sample in the right configuration (bending or torsion) and record the sound emited by your sample. The record duration is ''' + str(RECORDTIME) + ' s'
        self.status.set_msg(msg)
        self.init_record_param()
        self.sample = sample
        self.nav.help.bind(on_press=self.help)


    def help(self, obj):
        showHelpPage()

    def init_record_param(self):
        if platform == 'android':
            self.jnius = importlib.import_module('jnius')
            self.audiostream = importlib.import_module('audiostream')
            self.MediaRecorder =  self.jnius.autoclass('android.media.MediaRecorder')
            self.AudioSource =  self.jnius.autoclass('android.media.MediaRecorder$AudioSource')
            self.AudioFormat =  self.jnius.autoclass('android.media.AudioFormat')
            self.AudioRecord =  self.jnius.autoclass('android.media.AudioRecord')
        elif platform == 'linux':
            self.pyaudio = importlib.import_module('pyaudio')
        else:
            self.status.set_msg('Error: unsuported platform, please exit program')

    def next_screen(self, obj):
        self.manager.transition.direction = 'left'
        self.manager.current = 'Screen_2'

    def prev_screen(self, obj):
        self.manager.transition.direction = 'right'
        self.manager.current = 'Screen_0'

    def record(self, mode):
        self.frames = [] # the sound array
        self.sample.mode = mode

        if platform == 'android':
            Clock.schedule_once(self.start_record_android, 0)

        elif platform == 'linux':
            Clock.schedule_once(self.start_record_linux, 0)

        Clock.schedule_once(self.stop_record, RECORDTIME)
        wait_txt = 'Recording sound, please wait few seconds...'
        self.status.set_msg('Recording sound, please wait few seconds...')
        self.status.zoom(None, False)



    def start_record_linux(self, dt):
         p = self.pyaudio.PyAudio()
         stream = p.open(format=self.pyaudio.paInt16, channels=1, rate=RATE, input=True, frames_per_buffer=CHUNKSIZE)
         for _ in range(0, int(RATE / CHUNKSIZE * RECORDTIME)):
             data = stream.read(CHUNKSIZE)
             self.frames.append(np_fromstring(data, dtype=np_int16))

    
    def start_record_android(self, dt):
        self.ChannelConfig = self.AudioFormat.CHANNEL_IN_MONO
        self.AudioEncoding = self.AudioFormat.ENCODING_PCM_16BIT
        self.BufferSize = self.AudioRecord.getMinBufferSize(RATE, self.ChannelConfig, self.AudioEncoding)
        self.mic = self.audiostream.get_input(callback=self.mic_callback_android, source='mic', buffersize=self.BufferSize)
        self.count = 0
        self.mic.start()
        Clock.schedule_interval(self.readbuffer_android, 1./SAMPLE_PER_SECOND)

    def mic_callback_android(self, buf):
        self.frames.append(np_fromstring(buf, dtype=np_int16))

    def readbuffer_android(self, dt):
        self.mic.poll()

    def stop_record(self, dt):
        if platform == 'android':
            Clock.unschedule(self.readbuffer_android)
            self.mic.stop()

        try:
            # apply a fft to temporal signal
            result = np_hstack(self.frames)
            N = len(result)
            T = 1./RATE
            yf = np_fft(result)
            yf = 1./N * np_abs(yf[:N])
            xf = np_linspace(0.0, 1./T, N)
        
            # truncate value higher than 25000 Hz
            num_val = (25000./RATE)*N
            yf = yf[0:num_val]
            xf = xf[0:num_val]

            # select the frequence for max value 
            i = yf.argmax(axis=0)
            freq = xf[i]

            # give results
            msg = 'Detecting natural frequency at {:.1f} Hz. '.format(freq)
            if self.sample.mode == 'bending':
                E = self.sample.compute_young_modulus(freq)
                msg += 'The related Young\'s modulus is [b]{:.2f} GPa[/b]'.format(E*1e-9)
            else: 
                G = self.sample.compute_shear_modulus(freq)
                msg += 'The related shear modulus is [b]{:.2f} GPa[/b]'.format(G*1e-9)
            msg += '\nYou can go to the next step to fine tune this result' 
            self.status.set_msg(msg)

            sample.xf = xf
            sample.yf = yf
            sample.max_idx = i
            self.nav.button_next.disabled=False
            self.manager.get_screen('Screen_2').init_graph(None)
            self.status.zoom(None)



        except Exception, e:
            self.status.set_msg('Error during post-treatment of signal: \'' + str(e) + '\'')


class Screen_2(Screen):
    def __init__(self, sample, **kwargs):
        super(Screen_2, self).__init__(**kwargs)
        self.nav.button_prev.bind(on_press=self.prev_screen)
        self.nav.button_prev.disabled=False
        self.nav.button_next.disabled=True

        msg = 'Spectral analysis of sound. Use the <- -> buttons to measure some values. Click on the graph to customize plot.'
        self.status.set_msg(msg)
        self.sample = sample
        self.nav.help.bind(on_press=self.help)
        #self.bind(on_enter=self.init_graph)
        self.graph.bind(on_press=self.show_limit_dialog)
        
        self.plot = None
        self.line = None


    def init_graph(self, obj):
        if (self.plot is not None):
            self.graph.remove_plot(self.plot)
            self.graph.remove_plot(self.line)

        ymax = int(np_max(self.sample.yf))
        self.graph.ymax=ymax
        self.graph.y_ticks_major=ymax/5
        self.plot = MeshLinePlot(color=[1, 0, 0, 1])
        for i, val in enumerate(self.sample.xf):
            self.plot.points.append((self.sample.xf[i], self.sample.yf[i]))
        self.graph.add_plot(self.plot)
        
        self.current_line_index = sample.max_idx
        
        self.line = MeshLinePlot(color=[0, 1, 0, 1])
        x = self.sample.xf[self.current_line_index]
        y = self.sample.yf[self.current_line_index]
        self.line.points = [(x,0), (x, y)]
        self.graph.add_plot(self.line)


    def help(self, obj):
        showHelpPage()


    def prev_screen(self, obj):
        self.manager.transition.direction = 'right'
        self.manager.current = 'Screen_1'


    def show_limit_dialog(self, arg1):
        xmin_input_text = '{:.0f}'.format(self.graph.xmin)
        xmax_input_text = '{:.0f}'.format(self.graph.xmax)
        freq_input_text = '{:.0f}'.format(self.sample.xf[self.current_line_index])

        
        self.xmin_input = MyTextInput(input_type='number', multiline=False, text=xmin_input_text, id='xmin_input')
        self.xmax_input = MyTextInput(input_type='number', multiline=False, text=xmax_input_text, id='xmax_input')
        self.freq_input = MyTextInput(input_type='number', multiline=False, text=freq_input_text, id='xmax_input')

        xmin_label = Label(text='xmin value (Hz)', valign='bottom', halign='left')
        freq_label = Label(text='current measure (Hz)', valign='bottom', halign='left')
        xmax_label = Label(text='xmax value (Hz)', valign='bottom', halign='left')
        layout = GridLayout(cols=1, rows=6)        
        layout.add_widget(xmin_label)
        layout.add_widget(self.xmin_input)
        layout.add_widget(freq_label)
        layout.add_widget(self.freq_input)
        layout.add_widget(xmax_label)
        layout.add_widget(self.xmax_input)
        
        self.popup_xlim = Popup(title='Set plot values (press enter to update)',
                                content=layout, size_hint=(.6, None), height=sp(34*8))
        self.xmin_input.bind(on_text_validate=self.update_limits)
        self.freq_input.bind(on_text_validate=self.update_limits)
        self.xmax_input.bind(on_text_validate=self.update_limits)
        self.popup_xlim.open()


    def update_limits(self, obj):
        xmin_str = self.xmin_input.text
        xmax_str = self.xmax_input.text
        freq_str = self.freq_input.text
        try:
            xmin = int(float(xmin_str.split()[0]))
            xmax = int(float(xmax_str.split()[0]))
            freq = int(float(freq_str.split()[0]))
            self.graph.xmax=xmax
            self.graph.xmin=xmin
            self.move_val(freq_str)
            self.graph.x_ticks_major=(xmax-xmin)/5
            self.popup_xlim.dismiss()
            self.update_line()
        except ValueError:
            print 'problem'
    
    def previous_val(self):
        if self.current_line_index > 0:
            self.current_line_index = self.current_line_index - 1
            self.update_line()

    def previous_val_fast(self):
        self.current_line_index = self.current_line_index - 100
        if self.current_line_index <= 0:
            self.current_line_index = 0
        self.update_line()

    def next_val(self):
        if self.current_line_index < len(self.sample.yf):
            self.current_line_index = self.current_line_index + 1
            self.update_line()

    def next_val_fast(self):
        self.current_line_index = self.current_line_index + 100
        if self.current_line_index >= len(self.sample.yf):
            self.current_line_index = len(self.sample.yf) - 1
        self.update_line()


    def move_val(self, val_str):
        try:
            val = int(float(val_str))
            step = self.sample.xf[1] - self.sample.xf[0]
            index = val/step
            if index >= 0 and index < len(self.sample.xf):
                self.current_line_index = index
                
        except ValueError:
            print 'problem'

    def update_line(self):
        x = self.sample.xf[self.current_line_index]
        y = self.sample.yf[self.current_line_index]
        self.line.points = [(x,0), (x, y)]
        text = 'For x={:.2f} Hz, y={:.2f} db. '.format(x,y)
        if self.sample.mode == 'bending':
            E = self.sample.compute_young_modulus(x)
            text += 'The related Young\'s modulus [b]is {:.2f} GPa[/b]'.format(E*1e-9)
        else: 
            G = self.sample.compute_shear_modulus(x)
            text += 'The related shear modulus is [b]{:.2f} GPa[/b]'.format(G*1e-9)
        self.status.set_msg(text)
        self.current_line_val_input.text = '{:.2f} Hz'.format(x)


# Create the screen manager
sm = ScreenManager()
sample = Sample()
sm.add_widget(Screen_0(sample, name='Screen_0'))
sm.add_widget(Screen_1(sample, name='Screen_1'))
sm.add_widget(Screen_2(sample, name='Screen_2'))

class pyRfdaApp(App):

    def build(self):
        return sm
    
    def on_pause(self):
      # Here you can save data if needed
      return True

if __name__ == "__main__":
    client = Client('https://0a5611953c214e17b2c6de1c76f041d1:7e74d0cef57b4fd5978afa1ec65f18f0@sentry.io/103357')
    try:
        print 'DADA'
        pyRfdaApp().run()
    except:
        traceback.print_exc()
        ident = client.get_ident(client.captureException())
        print "Exception caught; reference is %s" % ident
